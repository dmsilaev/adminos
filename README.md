# Adminos

## Installation

Install initial adminos-structured files with

    $ rails g adminos:install

To add locales to installation:

    $ rails g adminos:install --locales=en,cn

Russian language is added and made the default one by default.

## Model generation

Generate adminos model with

    $ rails g adminos Model field:string body:text --type=sortable

Name, cached_slug, published, meta_description and meta_title fields are always added.

Available model types are: default(if no type is present), section, sortable and table.

Add sorting to field:

    $ rails g adminos Model field:string:sort --type=table

Type table is required for sorting to function properly. Sorting by name is added by default.

Add search form:

    $ rails g adminos Model body:text --search=name,body

Specify the fields to search in. It is performed using pg_search gem and directed towards quick word-based search solution, but can be customized further.

If the model will be in multiple locales, you can specify which fields to translate:

    $ rails g adminos Model field:string:locale body:text:locale

To translate default fields in case no locale specific custom fields are present, add --locale option:

    $ rails g adminos Model --locale
