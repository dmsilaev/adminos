Gem::Specification.new do |gem|
  gem.name          = 'adminos'
  gem.version       = '3.2.0'
  gem.authors       = ['RavWar']
  gem.email         = ['rav_war@mail.ru']
  gem.homepage      = ''
  gem.summary       = %q{Adminos molinos}
  gem.description   = %q{Molinos adminos}

  gem.files         = `git ls-files`.split($/)
  gem.test_files    = gem.files.grep %r{^spec/}
  gem.require_paths = %w(lib)

  gem.add_dependency 'path'
  gem.add_dependency 'tinymce-rails', '~> 4.0'
  gem.add_dependency 'tinymce-rails-langs', '~> 4.0'
  gem.add_dependency 'jquery-ui-rails'
  gem.add_dependency 'jquery-fileupload-rails'
  gem.add_development_dependency 'bundler', '~> 1.3'
  gem.add_development_dependency 'rails'
  gem.add_development_dependency 'rake'
end
