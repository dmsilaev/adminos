//= require tinymce-jquery
//= require jquery.ui.sortable
//= require jquery_nested_sortable
//= require admin/tinymce_config

$(function() {
  $('.with-nested-sortable > ul').withNestedSortable();
  $('a.ajax-post').ajaxRequestLink();
  $('a.ajax-delete').ajaxRequestLink({method: "delete"});
  $("form.method-link-to").addLinkToSubmitForm();

  // Togglable tabs <http://twitter.github.com/bootstrap/javascript.html#tabs>.
  $('.with-togglable-navigation .active a').tab('show');
  $('.with-togglable-navigation a').click(function (event) {
    event.preventDefault();
    $(this).tab('show');
  });
  $('.with-togglable-navigation *[data-toggle]').on('shown', function (event) {
    $(event.target).addClass('active'); // activated tab
    // event.relatedTarget; // previous tab
  })

  $('.with-move-to').sortable({
    handle: '.icon-move',
    stop: function(event, ui) {
      var to = ui.item.prev().data('id');
      var url = ui.item.data('href');
      $.post(url, {to :to})
    }
  });

  $('.with-apply-sortable-order[data-sortable]').sortable({
    opacity: 0.6,
    helper: function(event, el) {
      var content = '';
      var img = $(el).find('.sortable-helper');
      if (img.size() > 0) {
        content = "<img src='" + $(img).attr('src') + "'/>";
      } else {
        content = $(el).attr('data-placeholder');
      }
      return "<div class='helper' style='width: auto; height: auto;'>"+content+"</div>";
    },
    appendTo: 'body',
    handler: 'icon-move',
    items: $(this).attr('items'),
    stop: function(event, ui) {
      var url = $(this).attr('data-sortable');
      if (url) {
        var ids = $.map(
          $(this).children(),
          function(e, idx) {
            return $(e).attr('data-id');
          }
        );

        $.ajax({type: 'post', dataType: 'script', data: {id: ids}, url: url});
      }
    }
  });

  $("form a.submit-form, form input.delete-button, form.remote button")
    .on('click', function(event){
    event.preventDefault();
    var form = $(this).parents('form');
    var confirm_message = form.find('.confirm-message');
    if (confirm_message.length > 0) {
      if (!confirm(confirm_message.val())) {
        return false;
      }
    }
    form.submit();
    return false;
  });
});

$.fn.withNestedSortable = function(options) {
  var defaults = $.extend({}, $.fn.withNestedSortable.defaults, {
    maxLevels: $(this).attr('max-levels')
  });
  var settings = $.extend({}, defaults, options);

  $(this).nestedSortable({
    listType: 'ul',
    opacity: .8,
    items: 'li',
    tabSize: 32,
    maxLevels: settings.maxLevels,
    disableNesting: 'no-nesting',
    forcePlaceholderSize: true,
    placeholder: 'placeholder',
    tolerance: 'pointer',
    toleranceElement: '> div',
    handle: '.-move',
    start: function(event, ui) {
      $(ui.helper).addClass('dragging');
    },
    stop: function(event, ui) {
      var li = ui.item;
      li.removeClass('dragging');
      var id = li.attr('rel');
      var parent_id = li.parents('li').attr('rel');
      var prev_id = li.prev('li').first().attr('rel');
      if (parent_id == null) { parent_id = undefined }
      if (prev_id == null) { prev_id = undefined }

      data = {
        _method: 'PUT',
        prev_id: prev_id,
        parent_id: parent_id
      };

      $.ajax({
        async: false,
        type: 'POST',
        url: li.attr('href'),
        dataType: 'script',
        data: data
      });
    }
  });
}
$.fn.withNestedSortable.defaults = {
  maxLevels: 0
};

// AJAX request with authenticity_token (in caching case).
$.fn.ajaxRequestLink = function(options) {
  var defaults = {
    type: "POST",
    method: "post",
    confirmation: "Вы уверены?"
  };
  var settings = $.extend({}, defaults, options);
  $(this).on("click", function(event){
    event.preventDefault();
    var link = $(this);
    var token = $('meta[name=csrf-token]').attr("content");
    if (typeof token === 'string') {
      $.ajax({
        type: settings.type,
        url: link.attr("rel"),
        data: {
          authenticity_token: token,
          _method: settings.method
        },
        beforeSend: function(xhr) {
          if (link.is(".with-confirmation")) {
            if (window.confirm(settings.confirmation) != true) {return false}
          }
        },
        success: function(data) {
          if (link.is(".disable-after-success")) {
            link.replaceWith($('<span>' + link.text() + '</span>'));
          }
          // FIXME: Rewrite.
          // else if (link.is(".toggle")) {
          //   var oppositeLink = link.closest(".with-inline-css")
          //     .children().not(link.parent());
          //   link.parent().css("display","none");
          //   oppositeLink.show();
          // }
        }
      });
    } else {
      //console.log("AJAX request: meta authenticity_token not found!");
      return false;
    }
  });
}

$.fn.addLinkToSubmitForm = function() {
  if ($(this).size() > 0) {
    $(this).each(function(){
      var form = $(this);
      if (form.find("a.submit-form").size() === 0) {
        var link = $(
          '<a class="submit-form" href="' + form.attr('action') + '">'
            + form.attr('title')
            + '</a>'
        );
        form.append(link);
        form.attr("title", "");
      }
    });
  }
}
