(function() {
	var each = tinymce.each;

  tinymce.create('tinymce.plugins.GalleryPlugin', {
    init : function(ed, url) {
      ed.addCommand('mceGallery', function() {
        var selected = tinyMCE.activeEditor.selection.getContent(),
            starting = "&nbsp;<div class='gallery'><div class='royalSlider'>",
            bookmark = ed.selection.getBookmark(0),
            position = '<span id="'+bookmark.id+'_start" data-mce-type="bookmark" data-mce-style="overflow:hidden;line-height:0px"></span>';

        if (selected) {
          content = starting+selected+"</div></div>&nbsp;";
        } else {
          content = starting+position+"\uFEFF</div></div>&nbsp;";
        }

        tinymce.execCommand('mceInsertContent', false, content);
        ed.selection.moveToBookmark(bookmark);
      });

      // Register buttons - trigger above command when clicked
      ed.addButton('gallery', {title : 'Вставить галерею', cmd : 'mceGallery', icon : 'gallery' });
    },
  });

  tinymce.PluginManager.add('gallery', tinymce.plugins.GalleryPlugin);

})();


