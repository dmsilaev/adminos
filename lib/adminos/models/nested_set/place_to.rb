module Adminos::NestedSet::PlaceTo
  extend ActiveSupport::Concern

  def place_to(*args)
    options = args.extract_options!
    parent_id = args.first  || options.delete(:parent_id)
    prev_id   = args.second || options.delete(:prev_id)
    opts = {place_first: true}.merge(options)

    if parent_id
      parent = self.class.find(parent_id)
      self.move_to_child_of(parent)
    else
      self.move_to_root
    end

    if prev_id.present?
      prev = self.class.find(prev_id)
      self.move_to_right_of(prev)
    else
      if opts[:place_first] && self.parent && self.parent.children.count > 1
        self.move_to_left_of(self.parent.children.first)
      end
    end
    self.reload
    run_callbacks(:save)
  end

  def move_children_to_parent!
    self.children.each do |child|
      child.place_to(self.parent, nil)
      child.set_published_off if child.respond_to?(:set_published_off)
    end
  end
end
