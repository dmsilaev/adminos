require 'rails/generators/active_record'

module Adminos::Generators
  class AdminosGenerator < ActiveRecord::Generators::Base
    desc 'Creates defined model, migration and adds adminos-structured files'

    source_root File.expand_path '../../templates/adminos', __FILE__

    argument :attributes, type: :array, default: []
    hook_for :orm, required: true, as: :model

    class_option :type, type: :string
    class_option :locale, type: :boolean
    class_option :search, type: :string

    def extend_model
      model = "app/models/#{file_name}.rb"

      inject_into_file model, file_content('model_includes.rb', true), after: /ActiveRecord::Base\n/
      inject_into_file model, erb_file_content('model.rb', true), after: /attr_accessible.*\n/
      gsub_file model, /# attr_accessible :title, :body/, 'attr_accessible'
      inject_into_file model, columns, after: /attr_accessible/
    end

    def extend_migration
      file = Dir.glob('db/migrate/*.rb').grep(/\d+_create_#{table_name}.rb$/).first

      return unless file
      inject_into_file file, file_content('migration.rb', true), after: /create_table.*\n/
      if drag_type?
        inject_into_file file, "\n\n    add_index :#{table_name}, :parent_id", after: /    end/
      end
    end

    def extend_routes
      inject_into_file 'config/routes.rb', route_content, after: 'namespace :admin do'
    end

    def extend_navbar
      inject_into_file 'app/views/shared/admin/_navbar.haml', erb_file_content('navbar.haml'), after: /%ul.dropdown-menu.others\n/
    end

    def extend_locale
      # Encode because of a bug in thor
      inject_into_file 'config/locales/ru.yml', erb_file_content('ru.yml').force_encoding('ASCII-8BIT'), before: /.*helps:\n/
    end

    def create_controller
      template "types/#{actual_type}/controller.rb", "app/controllers/admin/#{table_name}_controller.rb"
    end

    def copy_views
      template 'fields.haml', "app/views/admin/#{table_name}/_fields.haml"
      directory "types/#{actual_type}/views", "app/views/admin/#{table_name}"
    end

    def locale_specific_actions
      return unless options.locale? or attributes.map(&:locale).any?

      template 'locales/locale_fields.haml', "app/views/admin/#{table_name}/_locale_fields.haml"
      template 'locales/general_fields.haml', "app/views/admin/#{table_name}/_general_fields.haml" #unless attributes.map(&:locale).delete_if{|e| e == true}.blank?
      remove_file "app/views/admin/#{table_name}/_fields.haml"
      inject_into_file "app/models/#{file_name}.rb", ', :translations_attributes', after: /.*attr_accessible.*/
      inject_into_file "app/models/#{file_name}.rb", erb_file_content('locales/model.rb'), after: /.*attr_accessible.*\n/
      inject_into_file "app/controllers/admin/#{table_name}_controller.rb", "            filter_by_locale: true,\n", before: /.*find_by_cached_slug.*\n/
      migration_template 'locales/migration.rb', "db/migrate/add_translation_table_to_#{file_name}"
    end

    def sort_specific_actions
      return unless attributes.map(&:sort).any? or actual_type == 'table'

      inject_into_file "app/views/admin/#{table_name}/index.haml", erb_file_content('sorts/headers.haml'), before: /.*\%th\.icon.*\n/
      inject_into_file "app/views/admin/#{table_name}/index.haml", erb_file_content('sorts/body.haml'), before: /.*\%td= object_link_edit\(object\).*\n/
    end

    def search_specific_actions
      return unless options.search?

      inject_into_file "app/models/#{file_name}.rb", "\n  scoped_search on: #{options.search.split(',').map{|s| s.strip.to_sym }}\n", after: /attr_accessible.*\n/
      inject_into_file "app/views/admin/#{table_name}/index.haml", "\n= render 'shared/admin/search_form'\n", after: /= collection_header\n/
      inject_into_file "app/controllers/admin/#{table_name}_controller.rb", '.search_for(params[:query])', after: /.*\|\|\= collection_orig/
    end

    protected

    def file_content file, type = nil
      prefix = type ? 'types/' + target_type + '/' : ''
      IO.read find_in_source_paths prefix + file
    end

    def erb_file_content file, type = nil
      ERB.new(file_content(file, type), nil, '-').result(instance_eval('binding'))
    end

    def actual_type
      options.type? ? options.type : 'default'
    end

    def target_type
      actual_type == 'table' ? 'default' : actual_type
    end

    def drag_type?
      options.type == 'section' or options.type == 'sortable'
    end

    def columns
      sum = %w(name cached_slug published meta_description meta_title)

      sum += %w(parent_id) if options.type == 'sortable'
      sum += %w(parent_id nav_published nav_name) if options.type == 'section'

      ' :' + sum.join(', :') + (attributes.any? ? ',' : '')
    end

    def route_content
      spaces = (options.locale? || attributes.map(&:locale).any? ? '      ' : '    ')

      if drag_type?
<<-eos

#{spaces}resources :#{table_name}, except: :show do
#{spaces}  collection { post :batch_action }
#{spaces}  member { put :drop }
#{spaces}end
eos
      else
<<-eos

#{spaces}resources :#{table_name}, except: :show do
#{spaces}  collection { post :batch_action }
#{spaces}end
eos
      end
    end
  end
end

# Extend rails to add locale instance variable to attribute
module Rails::Generators
  class GeneratedAttribute
    attr_accessor :locale, :sort

    def initialize_with_attributes name, type=nil, index_type=false, attr_options={}
      @locale = true if index_type == 'locale'
      @sort   = true if index_type == 'sort'
      initialize_without_attributes name, type, index_type, attr_options
    end
    alias_method_chain :initialize, :attributes
  end
end
