require 'bundler'
require 'rails/generators/active_record'

module Adminos::Generators
  class InstallGenerator < ActiveRecord::Generators::Base
    desc 'Creates initial adminos file and data structure'

    source_root File.expand_path '../../templates/install', __FILE__

    # ActiveRecord::Generators::Base requires a NAME parameter.
    argument :name, type: :string, default: 'random_name'

    class_option :locales, type: :string

    def remove_conflicts
      remove_dir 'test'
      remove_file 'Gemfile'
      remove_file '.gitignore'
      remove_file 'app/assets/javascripts/application.js'
      remove_file 'app/views/layouts/application.html.erb'
      remove_file 'app/controllers/application_controller.rb'
      remove_file 'config/database.yml'
      remove_file 'config/locales/en.yml'
      remove_file 'config/routes.rb'
      remove_file 'public/404.html'
      remove_file 'public/500.html'
      remove_file 'public/index.html'
    end

    def auto_copy
      directory 'auto', '.'
    end

    def copy_templates
      template 'application.haml', 'app/views/layouts/application.haml'
      template 'admin.haml', 'app/views/layouts/admin.haml'
      template 'database.yml', 'config/database.yml'
      template 'deploy.rb', 'config/deploy.rb'
      template 'routes.rb', 'config/routes.rb'
    end

    def modify_application_config
      gsub_file 'config/application.rb', /.*time_zone =.*/, "    config.time_zone = 'Moscow'"
      gsub_file 'config/application.rb', /.*default_locale.*/, '    config.i18n.default_locale = :ru'
      inject_into_file 'config/application.rb', file_content('application.rb'), after: /Rails::Application\n/
    end

    def locale_gem
      append_file 'Gemfile', file_content('locale/Gemfile') if options.locales?
    end

    def generators
      Bundler.with_clean_env { in_root { run 'bundle' }}
      in_root { run 'rake db:drop db:create' }
      in_root { run 'rails g devise:install' }
      in_root { run 'rails g paper_trail:install --with-changes' }
      in_root { run 'rails g devise User roles_mask:integer --no-routes' }
    end

    def edit_devise_config
      inject_into_file 'config/routes.rb', file_content('devise_routes_fragment.rb'), before: /.*root to: 'index.*/
      gsub_file 'config/initializers/devise.rb', /.*please-change-me-at-config-initializers-devise@example.com\"/, "  config.mailer_sender = Settings.get.email_header_from || 'admin@molinos.ru' rescue nil"
      gsub_file 'config/initializers/devise.rb', /.*# config.omniauth.*/, "  config.omniauth :facebook, '436680133113363', '99303f2141cf42f1a4fd96e101933751'\n  config.omniauth :twitter, 'test', 'test'\n  config.omniauth :vkontakte, 'test', 'test'"
    end

    def copy_user_model
      remove_file 'app/models/user.rb'
      copy_file 'user.rb', 'app/models/user.rb'
    end

    def migrations
      migration_template 'pages_migration.rb', 'db/migrate/create_pages'
      migration_template 'populate_users_migration.rb', 'db/migrate/populate_users'
      migration_template 'settings_migration.rb', 'db/migrate/create_settings'
      migration_template 'inline_images_migration.rb', 'db/migrate/create_inline_images'
      migration_template 'inline_attachments_migration.rb', 'db/migrate/create_inline_attachments'
      migration_template 'authentications_migration.rb', 'db/migrate/create_authentications'
    end

    def locale_specific_actions
      return unless options.locales?

      migration_template 'locale/add_translation_table_to_page.rb', 'db/migrate/add_translation_table_to_page'
      inject_into_file 'config/application.rb', "    config.i18n.fallbacks = true\n", after: /.*i18n.default_locale.*\n/
      inject_into_file 'config/application.rb', "    config.i18n.available_locales = #{options.locales.split(',').map{|s| s.strip.to_sym }.unshift(:ru)}\n", before: /.*i18n.default_locale.*\n/
      inject_into_file 'app/models/page.rb', file_content('locale/page.rb'), before: /.*BEHAVIORS = .*/
      inject_into_file 'app/models/page.rb', ', :translations_attributes', after: /.*:parent_id, :published/
      inject_into_file 'config/routes.rb', '  ', before: /  .*/, force: true
      inject_into_file 'config/routes.rb', "\n  scope '(:locale)', locale: /\#{I18n.available_locales.join('|')}/ do", after: /.*::Application.*/
      inject_into_file 'config/routes.rb', "\n  end", after: /.*root to: 'index#index'/
      inject_into_file 'app/controllers/admin/pages_controller.rb', "            filter_by_locale: true,\n", after: /.*[:sorted],\n/
      inject_into_file 'app/controllers/application_controller.rb', ', :set_locale', after: /.*before_filter :define_breadcrumb_struct/
      inject_into_file 'app/controllers/application_controller.rb', file_content('locale/controller.rb'), after: /.*protected\n/
      inject_into_file 'app/views/shared/admin/_navbar.haml', file_content('locale/navbar.haml'), after: /.*pull-right\n/
      inject_into_file 'app/controllers/admin/base_controller.rb', file_content('locale/base_controller.rb'), after: /  end\n/
      inject_into_file 'app/controllers/admin/base_controller.rb', "  around_filter :cleanup_globalize, only: [:create, :update]\n", after: /before_filter.*\n/
      remove_file 'app/views/admin/pages/_fields.haml'
      directory 'locale/auto', '.'
    end

    private

    def file_content file
      IO.read find_in_source_paths file
    end
  end
end
