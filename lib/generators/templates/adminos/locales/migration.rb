class AddTranslationTableTo<%= file_name.capitalize %> < ActiveRecord::Migration
  def up
    <%= file_name.capitalize %>.create_translation_table!({
      name: :string,<%= "\n      nav_name: :string," if options.type == 'section' %>
<% attributes.each do |attribute| -%>
      <%= "#{attribute.name}: :#{attribute.type},\n" if attribute.locale -%>
<% end -%>
      meta_description: :text,
      meta_title: :string
    }, {
      migrate_data: true
    })
  end

  def down
    <%= file_name.capitalize %>.drop_translation_table! migrate_data: true
  end
end
