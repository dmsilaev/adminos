class Admin::<%= table_name.camelize %>Controller < Admin::BaseController
  resource( <%= file_name.camelize %>,
            collection_scope: [:sorted],
            location: proc { polymorphic_path([:admin, resource.class]) },
            finder: :find_by_cached_slug! )

  private

  alias_method :collection_orig, :collection
  def collection
    @collection ||= collection_orig
      .page(params[:page]).per(settings.per_page)
  end
end
