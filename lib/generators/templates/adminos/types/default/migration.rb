      t.string   :name
      t.boolean  :published, default: true, null: false
      t.string   :cached_slug
      t.text     :meta_description
      t.string   :meta_title
