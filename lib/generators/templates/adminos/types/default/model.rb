
  has_paper_trail
  slugged :recognizable_name
  flag_attrs :published
  acts_as_recognizable :recognizable_name

  validates :name, presence: true

  scope :sorted, order('<%= "#{table_name}." if options.locales? %>created_at DESC')

  def reasonable_name
    name
  end

  def recognizable_name
    cached_slug.present? ? cached_slug : reasonable_name
  end
