
  has_paper_trail
  materialize_path
  acts_as_nested_set
  acts_as_recognizable :recognizable_name
  slugged :recognizable_name
  flag_attrs :published, :nav_published

  after_save :update_descendants_states

  validates :name, :nav_name, presence: true

  scope :sorted, order('lft ASC')
  scope :navigation, where(published: true, nav_published: true)
  scope :navigation_top, navigation.where(depth: 0)

  def reasonable_name
    name
  end

  def recognizable_name
    cached_slug.present? ? cached_slug : reasonable_name
  end

  def breadcrumbs
    ancestors.navigation
  end

  def absolute_path
    "/#{path}"
  end

  def update_descendants_states
    if published_changed? && !published?
      self.class.unscoped.where(parent_id: id).set_each_published_off
    end
  end

  alias_method :destroy_orig, :destroy
  def destroy
    safe_destroy(children_to: :parent, without_destroy: true)
    destroy_orig
  end
