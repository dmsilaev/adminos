class Admin::<%= table_name.camelize %>Controller < Admin::BaseController
  resource( <%= file_name.camelize %>,
            collection_scope: [:sorted],
            location: proc { polymorphic_path([:admin, resource.class]) },
            collection_scope: [:sorted],
            finder: :find_by_cached_slug! )
end
