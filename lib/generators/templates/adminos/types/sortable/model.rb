
  has_paper_trail
  slugged :recognizable_name
  flag_attrs :published
  acts_as_nested_set
  acts_as_recognizable :recognizable_name

  after_save :update_descendants_states

  validates :name, presence: true

  scope :sorted, order('lft ASC')

  def reasonable_name
    name
  end

  def recognizable_name
    cached_slug.present? ? cached_slug : reasonable_name
  end

  def update_descendants_states
    if published_changed? && !published?
      self.class.unscoped.where(parent_id: id).set_each_published_off
    end
  end

  alias_method :destroy_orig, :destroy
  def destroy
    safe_destroy(children_to: :parent, without_destroy: true)
    destroy_orig
  end
