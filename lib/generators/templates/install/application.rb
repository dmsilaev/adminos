    config.generators{|g| g.test_framework :rspec }
    config.exceptions_app = self.routes

    config.to_prepare do
      Devise::SessionsController.layout 'admin'
      Devise::RegistrationsController.layout proc{ |controller| user_signed_in? ? 'application' : 'admin' }
      Devise::ConfirmationsController.layout 'admin'
      Devise::UnlocksController.layout 'admin'
      Devise::PasswordsController.layout 'admin'
    end

    config.action_mailer.default_url_options = { host: 'molinos.ru' }

    config.assets.precompile += %w(
      app.js
      app.css
      error.css
      ie8.css
      ie9.css
      html5shiv.min.js
      printshiv.min.js
      respond.js
      admin/gallery_plugin.js
      admin/gallery.css
    )
