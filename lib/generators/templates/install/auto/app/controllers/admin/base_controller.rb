class Admin::BaseController < ApplicationController
  include Adminos::Controllers::AdminExtension
  layout 'admin/base'

  before_filter :authorize

  private

  def authorize
    authorize! :manage, :all
  end
end
