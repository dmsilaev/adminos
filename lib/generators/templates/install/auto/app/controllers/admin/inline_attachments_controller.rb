class Admin::InlineAttachmentsController < Admin::BaseController
  skip_before_filter :verify_authenticity_token, only: :create

  def create
    attachment = InlineAttachment.create file: params[:upload]

    file_type = attachment.file.file.content_type
    css_class = \
      case file_type
      when 'application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
        :doc
      when 'application/vnd.ms-excel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        :xls
      when 'application/zip',
        'application/x-7z-compressed',
        'application/x-tar',
        'application/gzip',
        'application/x-bzip2',
        'application/rar', 'application/x-rar-compressed'
      then
        :rar
      when 'application/pdf'
        :pdf
      when 'application/vnd.ms-powerpoint', 'application/vnd.openxmlformats-officedocument.presentationml.presentation'
        :ppt
      when 'image/gif',
        'image/jpeg',
        'image/pjpeg', #ie
        'image/x-png', #ie
        'image/png'
      then
        :img
      when 'application/x-shockwave-flash'
        :swf
      end

    url     = attachment.file.url
    width   = params[:width].blank? ? 320 : params[:width]
    height  = params[:height].blank? ? 240 : params[:height]
    @insert = \
      case css_class
      when :swf
        %Q[<object width="#{width}" height="#{height}" type="#{file_type}" data="#{attachment.file.url}"><param name="src" value="#{url}"></object>]
      else
        %Q[<a href="#{url}">#{attachment.file.file.filename}</a>]
      end

    render layout: false
  end
end
