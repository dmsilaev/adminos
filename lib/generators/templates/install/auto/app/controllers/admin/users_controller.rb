class Admin::UsersController < Admin::BaseController
  resource User, location: proc { polymorphic_path([:admin, resource.class]) }

  private

  alias_method :collection_orig, :collection
  def collection
    @collection ||= collection_orig.search_for(params[:query])
      .page(params[:page]).per(settings.per_page)
      .order("#{params[:order_by]} #{params[:direction]}")
  end
end
