class Admin::VersionsController < Admin::BaseController
  resource PaperTrail::Version,
    location: proc { polymorphic_path([:admin, resource.class]) }

  private

  alias_method :collection_orig, :collection
  def collection
    @collection ||= collection_orig.order('created_at desc')
      .page(params[:page]).per(settings.per_page)
  end
end
