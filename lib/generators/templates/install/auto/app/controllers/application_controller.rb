class ApplicationController < ActionController::Base
  protect_from_forgery

  respond_to :html

  before_filter :define_breadcrumb_struct, :check_routes_version

  @@routes_version = 0

  def self.update_routes version
    @@routes_version = version
    Rails.application.reload_routes!
  end

  protected

  rescue_from CanCan::AccessDenied do |exception|
    path = current_user ? root_path : new_user_session_path
    session[:previous_url] = request.fullpath
    redirect_to path, alert: exception.message
  end

  def after_sign_in_path_for resource
    session[:previous_url] || super
  end

  def define_breadcrumb_struct
    Struct.new('Breadcrumb', :label, :url) unless defined?(Struct::Breadcrumb)
  end

  def check_routes_version
    version = Rails.cache.fetch(:routes_version) { 0 }
    ApplicationController.update_routes(version) if version != @@routes_version
  end

  def paper_trail_enabled_for_controller
    ApplicationController.descendants.include? Admin::BaseController
  end
end
