class AuthenticationsController < Devise::OmniauthCallbacksController
  def all
    if find_auth && !current_user
      flash[:notice] = t('social.login_success', provider: find_auth.provider.capitalize)
      sign_in_and_redirect find_auth.user, event: :authentication
    elsif current_user
      return redirect_to :root, alert: t('social.already_bound') if find_auth

      current_user.authentications.create! omni_key_with_nickname
      redirect_to :root, notice: t('social.bind_success', provider: find_auth.provider.capitalize)
    elsif user_exists?
      new_auth.update_attribute :user, find_user
      find_user.confirm! unless find_user.confirmed?

      flash[:notice] = t('social.email_bind_success', provider: omni.provider.capitalize)
      sign_in_and_redirect new_auth.user, event: :authentication
    else
      new_auth.build_user(email: omni.info.email).skip_confirmation!
      new_auth.save! validate: false

      cookies[:link_auth_modal] = true
      flash[:notice] = t('social.signup_success', provider: new_auth.provider.capitalize)
      sign_in_and_redirect new_auth.user, event: :authentication
    end
  end

  alias twitter all
  alias facebook all
  alias vkontakte all

  def link
    user = User.find_by_email params[:email]

    return head 401 unless user and user.valid_password?(params[:password])

    auth = current_user.authentications.first
    auth.update_attribute :user, user
    current_user.destroy

    cookies.delete :link_auth_modal
    flash[:notice] = t('social.bind_success', provider: auth.provider.capitalize)

    sign_in :user, auth.user
    head :ok
  end

  private

  def omni
    request.env['omniauth.auth'].except('extra')
  end

  def omni_key
    @omni_key ||= { provider: omni.provider, uid: omni.uid.to_s }
  end

  def omni_key_with_nickname
    omni_key.merge({ name: omni.info.nickname })
  end

  def find_auth
    @auth ||= Authentication.where(omni_key).first
  end

  def new_auth
    @new_auth ||= Authentication.new params[:authentication] || omni_key
  end

  def find_user
    @user ||= User.where(email: omni.info.email).first
  end

  def user_exists?
    omni.info.email.present? and find_user
  end
end
