class Authentication < ActiveRecord::Base
  belongs_to :user
  attr_accessible :provider, :uid, :name, :user_attributes
  accepts_nested_attributes_for :user

  validates :uid, uniqueness: { scope: :provider }
end
