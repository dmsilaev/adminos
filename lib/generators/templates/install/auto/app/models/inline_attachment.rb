class InlineAttachment < ActiveRecord::Base
  attr_accessible :file

  mount_uploader :file, InlineAttachmentUploader

  validates :file, presence: true
end
