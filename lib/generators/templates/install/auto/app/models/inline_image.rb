class InlineImage < ActiveRecord::Base
  attr_accessible :image, :gallery

  mount_uploader :image, InlineImageUploader

  validates :image, presence: true

  def parse_input preview=nil
    image_url = preview ? image.url(:preview) : image.url
    %Q[<img src="#{image_url}" />]
  end
end
