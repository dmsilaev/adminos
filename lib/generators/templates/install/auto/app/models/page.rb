class Page < ActiveRecord::Base
  include Adminos::NestedSet::MaterializePath
  include Adminos::NestedSet::PlaceTo
  include Adminos::NestedSet::SafeDestroy
  include Adminos::Slugged
  include Adminos::Wysiwyg
  include Adminos::FlagAttrs

  attr_accessible :name, :nav_name, :nav_published, :behavior, :body, :cached_slug,
    :meta_description, :meta_title, :parent_id, :published

  BEHAVIORS = [
                'pages'
              ]

  has_paper_trail
  materialize_path
  acts_as_nested_set
  wysiwyg_sanitize :body
  slugged :recognizable_name
  flag_attrs :published, :nav_published

  after_save :update_descendants_states
  after_save :reload_routes, if: :reload_routes?

  validates :behavior, :name, :nav_name, presence: true

  scope :sorted, order('lft ASC')
  scope :for_routes, order('behavior ASC, lft DESC')
  scope :navigation, where(published: true, nav_published: true)
  scope :navigation_top, navigation.where(depth: 0).sorted
  scope :reverse_sorted, order('lft DESC')
  scope :with_behavior , proc { |b| where(behavior: b.to_s) }

  def reasonable_name
    name
  end

  def recognizable_name
    cached_slug.present? ? cached_slug : reasonable_name
  end

  def breadcrumbs
    ancestors.navigation
  end

  def absolute_path
    "/#{path}"
  end

  def update_descendants_states
    if published_changed? && !published?
      self.class.unscoped.where(parent_id: id).set_each_published_off #FIXME: Unscoped workaround. Descendants method always return empty collection
    end
  end

  alias_method :destroy_orig, :destroy
  def destroy
    safe_destroy(children_to: :parent, without_destroy: true)
    destroy_orig
  end

  def default_behavior?
    behavior == self.class.default_behavior
  end

  def editable_behavior?
    [self.class.default_behavior].include? behavior
  end

  def human_behavior_name
    I18n.t "#{self.class.table_name}.behaviors.#{behavior}"
  end

  def reload_routes
    version = Rails.cache.increment :routes_version
    ApplicationController.update_routes version
  end

  def default_route *actions
    [behavior, { path: absolute_path, only: actions, page_id: id }]
  end

  class << self
    def sitemap
      navigation.where('behavior != ? OR behavior IS NULL', 'sitemaps').arrange
    end

    def default_behavior
      'pages'
    end

    def human_behavior_name behavior
      I18n.t "pages.behaviors.#{behavior}"
    end
  end

  private

  def reload_routes?
    # Empty hash is for move operations
    cached_slug_changed? or behavior_changed? or self.changes == {}
  end
end
