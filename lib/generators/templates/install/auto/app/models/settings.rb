class Settings < ActiveRecord::Base
  attr_accessible :company_name, :contact_email, :copyright, :email,
    :email_header_from, :index_meta_description,
    :index_meta_title, :per_page, :seo_google_analytics, :seo_yandex_metrika

  before_validation :sanitize

  validates :company_name, :email, :email_header_from, :per_page, presence: true

  has_paper_trail

  def self.get
    first || new
  end

  private

  def sanitize
    fields = [ :copyright, :email, :index_meta_description, :index_meta_title ]

    fields.each do |attribute|
      self[attribute] = Sanitize.clean self[attribute], elements: ['br']
    end
  end
end
