class InlineImageUploader < BaseUploader
  include CarrierWave::MiniMagick

  process resize_to_limit: [1216, 1216], if: :gallery_image?

  version :preview do
    process resize_to_fill: [75, 75]
  end

  def extension_white_list
    %w(jpg jpeg gif png)
  end

  def gallery_image? file=nil
    model.gallery
  end
end
