before "#{application}:symlink", "#{application}:application_config"

namespace :"#{application}" do
  desc 'Deploy application yaml config file to shared path on server'
  task :application_config, :roles => :app do
    data = ERB.new(IO.read(File.dirname(__FILE__) + "/application.yml.erb")).result(binding)
    put data, "#{shared_path}/config/application.yml"
  end
end
