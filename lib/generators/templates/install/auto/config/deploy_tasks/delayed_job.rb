# after 'deploy:stop',    'delayed_job:stop'
# after 'deploy:start',   'delayed_job:start'
# after 'deploy:restart', 'delayed_job:restart'

namespace :delayed_job do
  desc 'Start the delayed_job process with god'
  task :start, roles: :app, except: { no_release: true } do
    run <<-EOF
      cd #{current_path} && RAILS_ENV=#{rails_env} \
        bundle exec god --port=#{god_port} \
                        --pid=#{god_pid} \
                        start #{god_application_identifier}_delayed_job
    EOF
  end

  desc 'Stop the delayed_job process with god'
  task :stop, roles: :app, except: { no_release: true } do
    run <<-EOF
      cd #{current_path} && RAILS_ENV=#{rails_env} \
        bundle exec god --port=#{god_port} \
                        --pid=#{god_pid} \
                        stop #{god_application_identifier}_delayed_job
    EOF
  end

  desc 'Restart the delayed_job process with god'
  task :restart, roles: :app, except: { no_release: true } do
    run <<-EOF
      cd #{current_path} && RAILS_ENV=#{rails_env} \
        bundle exec god --port=#{god_port} \
                        --pid=#{god_pid} \
                        restart #{god_application_identifier}_delayed_job
    EOF
  end
end
