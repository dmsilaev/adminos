
  # Needed to properly submit params with locale different from default
  # Remove blank globalize translations based on required NAME field
  def cleanup_globalize
    if defined? resource_params
      translation_params = params["#{resource_params}"][:translations_attributes]
    end

    if translation_params.present?
      translation_params.delete_if do |id, h|
        !h.values_at(*resource_class.required_translated_attributes).map do |v|
          v.empty? ? nil : v
        end.all?
      end

      Globalize.with_locale(I18n.default_locale) { yield }
    else
      yield
    end
  end
