<%= application_name.camelize %>::Application.routes.draw do
  namespace :admin do
    resources :helps, only: :index
    resource  :settings, only: [:edit, :update]

    resources :users, except: :show do
      collection { post :batch_action }
    end

    resources :versions, only: [:index, :show] do
      collection { post :batch_action }
    end

    resources :inline_attachments, only: :create
    resources :inline_images, only: [:new, :create, :destroy] do
      post 'add_item', on: :collection
    end

    resources :pages, except: :show do
      collection { post :batch_action }
      member { put :drop }
    end

    root to: 'pages#index'
  end

  root to: 'index#index'

  get '/404', to: 'errors#not_found'
  get '/500', to: 'errors#internal_error'

  begin
    Page.for_routes.group_by(&:behavior).each do |behavior, pages|
      pages.each do |page|
        case behavior
        when nil
        else
          resource( "#{page.class.name.underscore}_#{page.id}",
                    path:       page.absolute_path,
                    controller: behavior,
                    only:       :show,
                    page_id:    page.id )
        end
      end
    end
  rescue
    nil
  end
end
