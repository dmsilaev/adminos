require 'role_model'

class User < ActiveRecord::Base
  include RoleModel

  devise :database_authenticatable, :registerable, :recoverable,
    :rememberable, :trackable, :validatable, :omniauthable

  attr_accessible :roles, :roles_mask, :email, :password, :password_confirmation, :remember_me

  has_many :authentications, dependent: :destroy

  roles :admin
  scoped_search on: :email

  has_paper_trail skip: :remember_created_at

  def reasonable_name
    email
  end

  def translated_roles
    role_symbols.map{|s| self.class.human_attribute_name(s) }.join ', '
  end
end
