require 'test_helper'

class InstallLocaleGeneratorTest < Rails::Generators::TestCase
  destination DUMMY_LOCALE_PATH

  test 'Assert models' do
    assert_file 'app/models/page.rb', /translates :name, .*/
    assert_file 'app/models/page.rb', /:translations_attributes/
  end

  test 'Assert controllers' do
    assert_file 'app/controllers/admin/pages_controller.rb', /filter_by_locale: true,/
    assert_file 'app/controllers/application_controller.rb', /, :set_locale/
    assert_file 'app/controllers/application_controller.rb', /def default_url_options/
    assert_file 'app/controllers/admin/base_controller.rb', /def cleanup_globalize/
    assert_file 'app/controllers/admin/base_controller.rb', /around_filter :cleanup_globalize, only: \[:create, :update\]/
  end

  test 'Assert views' do
    assert_file 'app/views/admin/base/_fields.haml'
    assert_file 'app/views/admin/base/_pills.haml'
    assert_file 'app/views/admin/pages/_general_fields.haml'
    assert_file 'app/views/admin/pages/_locale_fields.haml'
    assert_file 'app/views/shared/admin/_navbar.haml', /I18n.available_locales.each do |locale|/
  end

  test 'Assert config' do
    assert_file 'config/initializers/globalize_fields.rb'
    assert_file 'config/routes.rb', /scope '\(:locale\)'/
    assert_file 'config/application.rb', /config.i18n.fallbacks = true/
    assert_file 'config/application.rb', /config.i18n.available_locales = \[:ru, :en, :nl\]/
  end

  test 'Assert Gemfile' do
    assert_file 'Gemfile', /gem 'globalize3'/
  end
end
